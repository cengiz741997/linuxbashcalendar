#!/bin/bash

title_cal="Mo Tu We Th Fr Sa Su"

space_start=""

current_date=$(date +"%d")


get_calendar () {

		local start_first_week=""
		declare -i local day_int="$(get_first_date_of_month)"
		declare -i local current_date=$(date +"%d")
		local display_week="" # string qui contient une par une les semaines des jours ex 5 7 8 9 10 ...

		if [ $current_date -eq 1 ];then
	  	 start_first_week=" "
		fi
		# permet de créer lespace dès le début pour arriver au bon jour
		# ex aujourd'hui nous somme vendredi
		#ex : mo tu we th fr sa su
		#                  1
		echo $day_int
      for (( c=1;c<$day_int;c++ ))
      do
	       start_first_week="   ${start_first_week}"
      done

		 # total de jour dans le mois
       declare -i local total_of_day=$(cal $(date +"%m %Y") | awk 'NF {DAYS = $NF}; END {print DAYS}')	
 		
 		 declare -i y=($day_int-1) # est la position du premier jour ex 3 pour mercredi
 		 echo -e " \n   Aujourd'hui nous somme le : $(date +"%d-%m-%Y")\n" # affichage du mois et l'année
 		 echo -e "   $(date +'%B') $(date +'%Y') \n"
		 echo $title_cal  # affichage des jours 
       for((c=1;c<=$total_of_day;c++))
       do
       
       	local day_string=$c

			local is_current_date_now="$(color_day $c)"    	
       	
       	if [ $is_current_date_now = 1 ];then # colorie en rouge le jour actuel
		 		day_string="\\e[0;41m${c}\\e[0m" 		 
		   fi
		 
			if [ $y -eq 7 ]; then
					echo -e "$display_week"
					y=0	
					display_week=""	
			fi
			
			if [ $c -gt 9 ]
				then
				 	display_week="${start_first_week}${display_week}${day_string} "
				 
				else
					display_week="${start_first_week}${display_week} ${day_string} "
			
			fi
			
			start_first_week=""	
			y+=1 
			
			if [ $c -eq $total_of_day  ]; then
					echo -e "$display_week"	
			fi
	       
       done


		 # is_current_date_now="$(color_day $count_day_number)"
		 # \\e[0;41m \\e[0m
 		 # start_first_week="${start_first_week}${count_day_number}" 		 
		 #if [ $is_current_date_now = 1 ];then
		 	# start_first_week="${start_first_week}\\e[0;41m${count_day_number}\\e[0m" 		 
		 # fi
}

# s'occupe de retourner la position du 1er jour
get_first_date_of_month() {
	local month_year=$(date +'%m %Y' | awk '!$1{$1=12;$2--}1')
	local m=${month_year% *}
	local y=${month_year##* }
	local d=$(cal $m $y | paste -s - | awk '{print $NF}')
	local first_date=$(printf '%s-%02s-01' $y $m)
	local -i day=$(date --date="${first_date}" +"%w") # ex retourne 3 pour mercredi
	echo $day
}

# retourne 1 si le jour actuel est l'argument passé $1 sont les mêmes
color_day() {
	
	day_compare=$1

	if [ $current_date = $day_compare ]; then
		echo 1
	else
		echo 0
	fi
}

get_calendar
